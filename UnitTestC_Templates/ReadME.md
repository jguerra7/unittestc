# Googletest setup for windows
```
Requirements MingW and maybe CMake

```

1. This Github repo contains libraries for google test. You can download the version of your choice.
Repo: https://github.com/google/googletest/tree/release-1.10.0


2. Download **libgtest.a** and **libgtest_main.a** libraries.
    (files are provided with this template)
3. Copy both these files into lib of MingW (Ex : C:\Program Files\mingw-w64\x86_64–8.1.0-win32-seh-rt_v6-rev0\mingw64\lib)

4. Go to Google test downloaded repo, extract it and navigate to: googletest →include →gtest [ex C:\Users\Downloads\googletest-release-1.10.0\googletest-release-1.10.0\googletest\include\gtest]

5. Copy that whole gtest file and copy to the folder MingW\lib\gcc\x86_64-w64-mingw32\8.1.0\include.



### You are all set to code! (At least that's the goal)


# Setup for Linux and MacOS

**CMake**
Install CMake 
options:
1. https://blog.eldernode.com/install-cmake-on-ubuntu-and-debian/
2. https://www.linuxfordevices.com/tutorials/install-cmake-on-linux
3. https://zwarrior.medium.com/install-google-test-framework-gtest-on-ubuntu-20-04-368eb6951b12



1. Start by installing the gtest development package:
```
sudo apt-get install libgtest-dev
```
2. These source files should be located at /usr/src/gtest. Browse to this folder and use cmake to compile the library:
```
sudo apt-get install cmake # install cmake
cd /usr/src/gtest
sudo cmake CMakeLists.txt
sudo make

# copy or symlink libgtest.a and libgtest_main.a to your /usr/lib folder
sudo cp *.a /usr/lib
```
