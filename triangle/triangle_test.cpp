#include "triangle.h"
#include <gtest/gtest.h>

namespace{
TEST(TriangleTest, InvalidSides){
    EXPECT_EQ(-1, TypeOfTriangle(-10,20,30));
    EXPECT_EQ(-1, TypeOfTriangle(10, -20,30));
    EXPECT_EQ(-1, TypeOfTriangle(3,4,-8));
}
}