**Commands to install GTest:**
```
sudo apt-get install libgtest-dev
```
**Commands to install CMake:**
```
sudo apt-get install cmake      
```

**Three commands to Running the tests:**
```

1. cmake CMakeLists.txt

2. make

3. ./runTests
```
