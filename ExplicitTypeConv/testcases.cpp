#include "TestCode.h"

#include <gtest/gtest.h>
//********************************
 
TEST(TypeConversion,displayresults) { 
  
    ASSERT_EQ(12, typeconv(4.5, 4.6, 4.9));
    ASSERT_EQ(20, typeconv(5.5, 4.6, 11.9));
 } 
TEST(TypeConversion, NoneNos) {
    ASSERT_EQ(0, typeconv(0.5, 0.6, 0.9));
}
//************************************
 


 
int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}