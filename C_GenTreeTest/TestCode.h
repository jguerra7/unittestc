#pragma once

#include <stdio.h>

struct TreeNode                  
{
    int parent_number;
    int number;
    struct TreeNode *firstChild ;
    struct TreeNode *nextSibling ;
};

#ifdef __cplusplus
extern "C" {
#endif
    struct TreeNode *buildTree(int [], int);

#ifdef __cplusplus
}
#endif
