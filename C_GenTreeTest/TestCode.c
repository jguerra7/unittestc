/******************************************************************************
This problem will create a general tree data structure using the TreeNode struct
defined below and included in the Testcode.h file.  For this particular tree 
there will be only three levels:

   1. Root - this is the root node that points to the first of possibly several 
             Level 1 nodes.
   2. Level 1 nodes - There can be zero to many Level 1 nodes directly under the root
                     node (considered children of the root node).  
                     Each of these Level 1 nodes can have zero to many children
                     nodes at Level 2.
                     
   3. Level 2 nodes - There can be zero to many child nodes for each of the Level 1 
                      nodes. These nodes will have NO children.
                    
All children nodes are related through a sibling relationship as defined as
"nextSibling" pointer in the TreeNode struct below. (see illustration)

Nodes that are parents, will point to the first child of the children nodes via
the "firstChild" pointer. (see illustration)

Nodes are placed in the tree using the following rules:

  1.  If a node has a parent_number of 0, then it will be placed in Level 1 of the tree.
      0 indicating that the root is its parent.
  2.  If a node has a parent_number > 0, then it will be placed in Level 2 under the 
      node in Level 1 containing that parent number.
	  
For example, if a node is added to the tree with parent_number = 0 and number = 7, it goes
into Level 1.  If the next node added has a parent_number of 7 and number of 33, then it goes
into Level 2 of the tree under the node that has number=7.
                    
A populated tree may look something like this (the Node numbers are simply random
labels):
                       --------
Root                   | root |
                       --------
                        /        
                       / firstChild
                      /
                     /
           Node 1   /                 Node 2                 Node 3      
            --------                ---------               --------
Level 1    |p = 0  | ------------->|p = 0   |-------------->|p = 0 |
           |n = 44 |  nextSibling  |n = 22  |  nextSibling  |n = 7 |
           ---------               ---------               ---------
             /                                                     \
            / firstChild                                            \ firstChild
           /                                                         \
          /                                                           \
         /  Node 4              Node 5                   Node 7        \ Node 6                 Node 8
        --------                ---------               ---------      ---------               --------
Level 2 |p = 44 | ------------->|p = 44 |-------------->|p = 44 |      | p = 7 |-------------->|p = 7 |
        |n = 10 |  nextSibling  |n = 17 |  nextSibling  |n = 2  |      | n = 33|  nextSibling  |n = 1 |
        ---------               ---------               ---------      ---------               --------



struct TreeNode                  
{
    int parent_number;
    int number ;
    struct TreeNode * firstChild ;
    struct TreeNode * nextSibling ;
};

TASK #1

Write the function buildTree that takes two parameters:

    1. An array of integers (nums)
    2. The size of the array (size)

The nums array will be used to construct TreeNode structures to be inserted into a tree.
The even indicies, nums[0], nums[2], nums[4] etc will hold parent_numbers, whereas the odd
indicies will hold numbers for each node.  So, nums[0] and nums[1] will be used to create a
node, the nums[2] and nums [3] for the next node etc. The tree should be built as you
iterate through the array.

Assumptions:
1. The array will always have an even number of items.
2. 'size' will always match the size of the array
3. No duplicate values for 'number' will be in the array
4. Nodes with a parent number > 0 will always have a matching parent node.

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "Testcode.h"


struct TreeNode * addNode(struct TreeNode * newNode, struct TreeNode * root)
{
    struct TreeNode *temp;
    if (newNode->parent_number == 0)
    {
        if(root == NULL)
        {
            root = newNode;
        }
        else
        {
            temp = root;
            while (temp->nextSibling != NULL)
                temp = temp->nextSibling;
            temp->nextSibling = newNode;
        }
    }
    else
    {
        temp = root;
        while(temp->number != newNode->parent_number)
        {
            temp = temp->nextSibling;
        }
        if(temp->firstChild == NULL)
        {
            temp->firstChild = newNode;
        }
        else
        {
            temp = temp->firstChild;
            while (temp->nextSibling != NULL)
                temp = temp->nextSibling;
            temp->nextSibling = newNode;
        }
    }
    return root;
}

struct TreeNode * buildTree(int nums[], int size)
{
    struct TreeNode* root = NULL, * temp;
    for(int i = 0; i < size; i += 2)
    {
        temp = (struct TreeNode*) malloc(sizeof(struct TreeNode));
        temp->parent_number = nums[i];
        temp->number = nums[i+1];
        temp->nextSibling = NULL;
        temp->firstChild = NULL;
        printf("i is %d\n",nums[i]);
        root = addNode(temp, root);
    }
    return root;
}

