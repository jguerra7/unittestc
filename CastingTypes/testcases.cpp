#include "TestCode.h"

#include <gtest/gtest.h>
 
TEST(CastTypechange,PositiveNums) { 
    ASSERT_EQ(10, change(10));
    ASSERT_EQ(70, change(70));
}
 TEST(CastTypechange,SumNums) { 
    ASSERT_EQ(108, changeone(1,'k'));
    ASSERT_EQ(109, changeone(2,'k'));
}

 TEST(CastTypechange,floatNums) { 
    ASSERT_EQ(120.000000, changetwo(13,'k'));
    ASSERT_EQ(121.000000, changetwo(14,'k'));
}
 
int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}