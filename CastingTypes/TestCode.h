#pragma once

#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif
    int change(int x);
    int changeone(int x, char z);
    int changetwo(int x, char z);
#ifdef __cplusplus
}
#endif